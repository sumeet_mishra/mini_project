import { Component, OnInit,Inject } from '@angular/core';
import{ObservService} from "../observ.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  tot_cart_items
  constructor(@Inject(ObservService) public objser) { 
this.objser.changemessgage.subscribe(dt=>{
  this.tot_cart_items=dt;
})
  }
ngOnInit() {
  }

}
