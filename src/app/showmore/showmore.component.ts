import { Component, OnInit,Inject } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Http} from "@angular/http";
import {ObservService} from "../observ.service";

@Component({
  selector: 'app-showmore',
  templateUrl: './showmore.component.html',
  styleUrls: ['./showmore.component.css']
})
export class ShowmoreComponent implements OnInit {
  select_prod_id;cur_items
  constructor(@Inject(ObservService) public obs,@Inject(ActivatedRoute) public ar,@Inject(Http) public ht,){
    this.obs.changemessgage.subscribe(dt=>{
      this.cur_items=dt
    })
    
    //-----ActivatedRoute concept to get  prod_id parameter in url--------------//
    this.ar.params.subscribe(dt =>{
     this.select_prod_id =(dt['prod_id'])
    })
  }
  ngOnInit(){
    this.funget()
  }
  
  //incrementing the add to cart value-----------//
  funadd(){
    this.cur_items=parseInt(this.cur_items)+1
    this.obs.fu_update(this.cur_items)
  }
  
  //-----to get the showmore product info-------------//
  one_prod_info;
  funget(){
    //alert("Executed")
     this.ht.post("getproduct_info",{product_id:this.select_prod_id}).subscribe(dt=>{
     this.one_prod_info =JSON.parse(dt._body)
    })
  }
}
