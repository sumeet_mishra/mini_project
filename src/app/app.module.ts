import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from "@angular/http"
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';
import { FooterComponent } from './footer/footer.component';
import { ShowmoreComponent } from './showmore/showmore.component';
import {RouterModule} from "@angular/router";
var robj=[{
  path:"",component:ContentComponent
},{
  path:"cont",component:ContentComponent
},{
  path:"more",component:ShowmoreComponent
}]
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentComponent,
    FooterComponent,
    ShowmoreComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,HttpModule,RouterModule.forRoot(robj)
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
