import { Component, OnInit,Inject } from '@angular/core';
import{Http} from "@angular/http";

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  constructor(@Inject (Http) public ht) {}
  
  pro_data;
  ngOnInit() {
    // to get documents from database
    this.ht.get("getdata").subscribe(dt =>{
      this.pro_data =JSON.parse(dt._body)
    })
  }

}
